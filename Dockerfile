FROM lorisleiva/laravel-docker:8.0

LABEL version="8.0"

ENV PATH="/usr/local/go/bin:${PATH}"
COPY --from=golang:1.17-alpine /usr/local/go/ /usr/local/go/
RUN ["apk", "add", "chromium-chromedriver"]
RUN ["apk", "add", "chromium"]
